// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches UserService service to the Fitly module
    angular
        .module("fitly")
        .service("UserService", UserService);

    // Inject $http to use built-in service to communicate with server
    UserService.$inject = ['$http'];

    // UserService function declaration
    function UserService($http) {

        // Declare 'service' and assigns it the object 'this'
        // Any function or variable attached to userService will be exposed to callers of
        // UserService, e.g. search.controller.js and register.controller.js
        var service = this;

        // EXPOSED DATA MODELS -------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------
        service.insertUser = insertUser;
        service.retrieveUserById = retrieveUserById;
        service.updateUser = updateUser;
        service.deleteUser = deleteUser;
        service.retrieveUsers = retrieveUsers;     
        service.loginUser = loginUser;
        service.logoutUser = logoutUser;

        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------
        // insertUser uses HTTP POST to send user information to the server's /users route
        // Parameters: user information; Returns: Promise object
        function insertUser(newUser) {
            // This line returns the $http to the calling function
            // This configuration specifies that $http must send the user data received from the calling function
            // to the /users route using the HTTP POST method. $http returns a promise object. In this instance
            // the promise object is returned to the calling function
            return $http({
                method: 'POST',
                url: 'api/users',
                data: {user: newUser}
            });
        };
        
        // retrieveUserById retrieves specific User information from the server via HTTP GET.
        // Parameters: userId. Returns: Promise object
        function retrieveUserById(userId) {
            return $http({
                method: 'GET',
                url: 'api/users/' + userId
            });
        };

        // updateUser updates a specific User information from the server via HTTP GET.
        // Parameters: user object. Returns: Promise object
        function updateUser(updatedUser) {
            return $http({
                method: 'PUT',
                url: 'api/user/' + updateUser.userId,
                data: {
                    user: updateUser
                }
            });
        };

        // deleteUser deletes a user by userId
        // Parameters: userId. Returns: Promise object!
        function deleteUser(userId) {
            return $http({
                method: 'DELETE',
                url: 'api/users/' + userId
            });
        };

        // retrieveUsers retrieves user information from the server via HTTP GET
        // Passes information via the query string (params)
        // Parameters: searchString. Returns: Promise object
        function retrieveUsers(searchString){
            return $http({
                method: 'GET',
                url: 'api/users',
                params: {
                    'keyword': searchString
                }
            });
        };
        
        // loginUser authenticates the user against server details
        // Parameters: userProfile (email & password). Returns: Promise object!
        function loginUser(userProfile) {
            return $http({
                method: 'POST',
                url: '/api/users/auth',
                data: {user: userProfile}
            });
        };

        // logoutUser logouts user out of the current session
        // Parameters: none. Returns: Promise object!
        function logoutUser() {
            return $http({
                method: 'GET',
                url: '/api/users/signout'
            });
        };

    }; // end of UserService
})();